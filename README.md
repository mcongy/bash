# bash

## About
This package is a set of command line very useful to manage servers. In includes bin/go binaries to go quicker in configuration / log files of each component.

## How to install 
To install it on each server you want, execute the following command : 

    $ cd ~ ; wget -c https://bitbucket.org/mcongy/bash/downloads/bash_release_latest.tar -O - | tar -x && source .bash_profile

This script will download and replace your bash profile & bash_rc file. Backup them if you want to avoid losing some beautiful things ! 