# Splash Screen



# Available functions

## Short aliases

### ls family
- alias **lx**='ls -lXB'         
    Sort by extension
- alias **lk**='ls -lSr'
    Sort by size, biggest last.
   
- alias **lt**='ls -ltr'
    Sort by date, most recent last.
    
- alias **lc**='ls -ltcr'
     Sort by/show change time,most recent last.
- alias **lu**='ls -ltur'
    Sort by/show access time,most recent last.

### Greps
- alias **h**=history
- alias **j**=list all jobs

### Paths
- alias **..**=='cd ../' 
     go upper in the path (1 hierarchical tree)
	 
- alias **.1**='cd ../'
     go upper in the path (1 hierarchicall level)

- alias **.2**='cd ../../'
     go upper in the path (2 hierarchical levels)
	 
- alias **.3**='cd ../../../'
     go upper in the path (3 hierarchical levels)	 
	 
- alias **lfx**='cd /opt/lfx-current/ ;  clear ; ll'
     display lfx root content directories and files.

- alias **home**='cd ~/'
     go back to the user directory.