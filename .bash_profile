# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs


PATH=$PATH:$HOME/bin
export PATH

source ~/.bash_completion.d/go
cd /opt/lfx-current 
# Execute some functions
my_ip
__external_ip
clear; __bash_profile__splash_screen ; ll

# Set Prompt
PS1="\`echo '\n$GREEN[\h]$NC[$MY_IP][$INET_IP]\n$YELLOW[\t] $LIGHTBLUE[\w] $LIGHTRED$ $NC'\` "